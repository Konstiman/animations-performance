import React, { useState } from "react";
import { timer } from "d3-timer";

const Current = ({duration, length}) => {
  const [x, setX] = useState(0);
  const [running, setRunning] = useState(false);

  const animate = () => {
    setRunning(true);
    let t = timer((elapsed) => {
      if (elapsed > duration) {
        t.stop();
        setRunning(false);
      }
      let ratio = elapsed > duration ? 1 : (1.0 * elapsed) / duration;
      setX(length * ratio);
    });
  };

  const reset = () => {
    setRunning(true);
    let t = timer((elapsed) => {
      if (elapsed > duration) {
        t.stop();
        setRunning(false);
      }
      let ratio = elapsed > duration ? 1 : (1.0 * elapsed) / duration;
      setX(length * (1 - ratio));
    });
  };

  return (
    <div>
      <div style={{ width: "100%" }}>
        <svg width={length + 161} height={100}>
          <g>
            <rect x={x} width="161" height="100" fill="#0080ff"></rect>
          </g>
        </svg>
      </div>
      <div>
        <button onClick={animate} disabled={running || x > 0}>
          Start
        </button>
        <button onClick={reset} disabled={running || x === 0}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default Current;
