import React, { useState } from "react";

const Css = ({ duration, length }) => {
  const [x, setX] = useState(0);
  const [running, setRunning] = useState(false);

  const animate = () => {
    setRunning(true);
    setX(length);

    setTimeout(() => {
      setRunning(false);
    }, duration);
  };

  const reset = () => {
    setRunning(true);
    setX(0);

    setTimeout(() => {
      setRunning(false);
    }, duration);
  };

  return (
    <div>
      <div style={{ width: "100%" }}>
        <svg width={length + 161} height={100}>
          <g>
            <rect className="rectangle" style={{x}} width="161" height="100" fill="#ffbf00" />
          </g>
        </svg>
      </div>
      <div>
        <button onClick={animate} disabled={running || x > 0}>
          Start
        </button>
        <button onClick={reset} disabled={running || x !== length}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default Css;
