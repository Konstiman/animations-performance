import React, { useState } from "react";

// inspired by https://css-tricks.com/guide-svg-animations-smil/
const Svg = ({ duration, length }) => {
  const [x, setX] = useState(0);
  const [running, setRunning] = useState(false);

  let animateRef = null;

  const animate = () => {
    setRunning(true);

    animateRef.setAttribute("from", 0);
    animateRef.setAttribute("to", length);
    animateRef.beginElement();

    setTimeout(() => {
      setRunning(false);
      setX(length);
    }, duration);
  };

  const reset = () => {
    setRunning(true);

    animateRef.setAttribute("from", length);
    animateRef.setAttribute("to", 0);
    animateRef.beginElement();

    setTimeout(() => {
      setRunning(false);
      setX(0);
    }, duration);
  };

  return (
    <div>
      <div style={{ width: "100%" }}>
        <svg width={length + 161} height={100}>
          <g>
            <rect id="rect" x={x} width="161" height="100" fill="#bfff00">
              <animate
                ref={(animate) => { animateRef = animate; }}
                begin="indefinite"
                attributeType="SVG"
                attributeName="x"
                from={0}
                to={length}
                dur={`${duration}ms`}
                fill="freeze"
              />
            </rect>
          </g>
        </svg>
      </div>
      <div>
        <button onClick={animate} disabled={running || x > 0}>
          Start
        </button>
        <button onClick={reset} disabled={running || x !== length}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default Svg;
