import React from "react";
import "./App.css";

import CurrentAnimation from "./components/Current";
import SvgAnimation from "./components/Svg";
import CssAnimation from "./components/Css";

export const DURATION = 2000;
export const LENGTH = 800;

function App() {
  return (
    <div className="App">
      <h2>Animations Performance Comparison</h2>
      <div>
        <h3>Recalculating with d3-timer</h3>
        <CurrentAnimation duration={DURATION} length={LENGTH} />
      </div>
      <div>
        <h3>SVG animate</h3>
        <SvgAnimation duration={DURATION} length={LENGTH} />
      </div>
      <div>
        <h3>CSS transition</h3>
        <CssAnimation duration={DURATION} length={LENGTH} />
      </div>
    </div>
  );
}

export default App;
