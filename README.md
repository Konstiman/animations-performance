# Animations Performance Comparison

This project was scripted fast to provide a simple comparison of different animation techniques.

You can find the live demo on surge: [http://okm-animations-performance.surge.sh/](http://okm-animations-performance.surge.sh/)

## Run

Or you can serve it locally like this:

```
yarn install
yarn start
```

This will run the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## About

The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
